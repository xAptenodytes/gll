CREATE TABLE IF NOT EXISTS cities (
    id serial,
    name text,
    CONSTRAINT cities_pkey PRIMARY KEY (id)
);

CREATE TABLE IF NOT EXISTS oauth_user_credentials (
    id serial,
    vk_id integer,
    CONSTRAINT oauth_user_credentials_pkey PRIMARY KEY (id)
);

CREATE TABLE IF NOT EXISTS roles (
    id serial,
    name text,
    CONSTRAINT roles_pkey PRIMARY KEY (id)
);

CREATE TABLE IF NOT EXISTS gll_users (
    id uuid PRIMARY KEY,
    birthday date,
    email text,
    first_name text,
    last_name text,
    patronymic text,
    phone text,
    sex integer,
	  city_ref_id integer,
  	role_ref_id integer,
  	oauth_credentials_ref_id integer,
    CONSTRAINT fk_city_id FOREIGN KEY (city_ref_id) REFERENCES cities (id),
    CONSTRAINT fk_role_id FOREIGN KEY (role_ref_id) REFERENCES roles (id),
    CONSTRAINT fk_oauth_credentials_id FOREIGN KEY (oauth_credentials_ref_id)
	  REFERENCES oauth_user_credentials (id)
);

CREATE TABLE IF NOT EXISTS vk_tokens (
    id serial,
    device_identifier text,
    vk_token text,
    is_expired boolean NOT NULL DEFAULT FALSE,
	  oauth_credentials_ref_id integer,
    CONSTRAINT fk_oauth_credentials_id  FOREIGN KEY (oauth_credentials_ref_id)
	  REFERENCES oauth_user_credentials (id),
    CONSTRAINT vk_tokens_pkey PRIMARY KEY (id)
);

CREATE SEQUENCE IF NOT EXISTS hibernate_sequence
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;