package ru.gll.backend.utils;

import lombok.Data;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.context.annotation.Configuration;

@Configuration
@EnableConfigurationProperties
@ConfigurationProperties("gllconfig")
@Data
public class GLLConfig {
    private Integer vkAppId;
    private String vkClientSecret;
}
