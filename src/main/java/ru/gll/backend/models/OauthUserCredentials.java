package ru.gll.backend.models;

import com.vk.api.sdk.objects.users.UserXtrCounters;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.util.Collections;
import java.util.List;

@Entity
@Table(name = "oauth_user_credentials")
@Data
@NoArgsConstructor
@AllArgsConstructor
public class OauthUserCredentials {
    public OauthUserCredentials(UserXtrCounters userXtr, String token, String deviceId) {
        this.vkId = userXtr.getId();
        VkToken vkToken = new VkToken();
        vkToken.setToken(token);
        vkToken.setDeviceIdentifier(deviceId);
        vkToken.setIsExpired(false);
        this.vkTokens = Collections.singletonList(vkToken);
    }

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Basic(optional = false)
    @Column(name = "id")
    private Integer id;

    @Column(name = "vk_id")
    private Integer vkId;

    @OneToMany(cascade = CascadeType.ALL, fetch = FetchType.EAGER)
    @JoinColumn(name = "oauth_credentials_ref_id")
    private List<VkToken> vkTokens;

    /*
        потенциально сюда добавится номер телефона и другие id и oauth токены
     */

    @OneToOne(cascade = CascadeType.ALL, mappedBy = "oauthUserCredentials", fetch = FetchType.LAZY)
    private GllUser gllUser;
}
