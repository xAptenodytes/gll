package ru.gll.backend.models;

import com.vk.api.sdk.objects.base.Sex;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.hibernate.annotations.GenericGenerator;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;

import javax.persistence.*;
import java.time.LocalDate;
import java.util.*;

@Entity
@Table(name = "gll_users")
@Data
@NoArgsConstructor
public class GllUser implements UserDetails {
    public GllUser(OauthUserCredentials oauthUserCredentials, String firstName, String lastName, Sex sex) {
        //TODO add roles
        this.oauthUserCredentials = oauthUserCredentials;
        this.role = new Role(1, "ORDINARY");
        this.firstName = firstName;
        this.lastName = lastName;
        this.sex = sex;
    }

    @Id
    @GeneratedValue(generator = "UUID")
    @GenericGenerator(name = "UUID",
            strategy = "org.hibernate.id.UUIDGenerator"
    )
    @Column(name = "id", updatable = false, nullable = false)
    private UUID id;

    @OneToOne(cascade = CascadeType.ALL, fetch = FetchType.EAGER)
    @JoinColumn(name = "oauth_credentials_ref_id", referencedColumnName = "id")
    private OauthUserCredentials oauthUserCredentials;

    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "role_ref_id")
    private Role role;

    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "city_ref_id")
    private CityModel city;
    /**
     * имя пользователя
     */
    @Column(name = "first_name")
    private String firstName;

    /**
     * имя пользователя
     */
    @Column(name = "patronymic")
    private String patronymic;

    /**
     * фамилия пользователя
     */
    @Column(name = "last_name")
    private String lastName;

    /**
     * пол. Возможные значения
     * 1 — женский;
     * 2 — мужской;
     * 0 — пол не указан.
     */
    @Column(name = "sex")
    private Sex sex;

    @Column(name = "birthday")
    private LocalDate birthday;

    /**
     * 79876543210 (только числа, без разделителей)
     */
    @Column(name = "phone")
    private String phone;

    @Column(name = "email")
    private String email;

    /**
     * @return random uuid; STAB
     */

    public Collection<? extends GrantedAuthority> getAuthorities() {
        return Collections.singletonList(role);
    }

    public String getPassword() {
        return null;
    }

    public String getUsername() {
        return oauthUserCredentials.getVkId().toString();
    }

    public boolean isAccountNonExpired() {
        return true;
    }

    public boolean isAccountNonLocked() {
        return true;
    }

    public boolean isCredentialsNonExpired() {
        return true;
    }

    public boolean isEnabled() {
        return true;
    }
}
