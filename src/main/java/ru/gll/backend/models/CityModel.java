package ru.gll.backend.models;

import lombok.Data;
import lombok.NoArgsConstructor;
import ru.gll.backend.swagger.models.City;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.List;

@Data
@Entity
@Table(name = "cities")
@NoArgsConstructor
public class CityModel {

    public CityModel(City city) {
        this.id = city.getCityId();
        this.name = city.getCityName();
    }

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Basic(optional = false)
    @Column(name = "id")
    private Integer id;

    @Column(name = "name")
    private String name;

    @OneToMany(mappedBy = "city")
    private List<GllUser> gllUsers = new ArrayList<>();

    @Transient
    public City getSwaggerCity() {
        return new City().cityId(id).cityName(name);
    }
}
