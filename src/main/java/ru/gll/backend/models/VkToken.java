package ru.gll.backend.models;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;

@Entity
@Table(name = "vk_tokens")
@Data
@NoArgsConstructor
@AllArgsConstructor
public class VkToken {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "id")
    private Integer id;

    @Column(name = "vk_token")
    private String token;

    @Column(name = "device_identifier")
    private String deviceIdentifier;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "oauth_credentials_ref_id", referencedColumnName = "id")
    private OauthUserCredentials oauthUserCredentials;

    @Column(name = "is_expired", nullable = false, columnDefinition = "boolean default false")
    private Boolean isExpired;
}
