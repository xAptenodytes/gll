package ru.gll.backend.security;

import io.jsonwebtoken.JwtBuilder;
import io.jsonwebtoken.Jwts;
import io.jsonwebtoken.SignatureAlgorithm;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import ru.gll.backend.models.GllUser;
import ru.gll.backend.services.GllUserDetailsService;

import java.time.*;
import java.util.Calendar;
import java.util.HashMap;
import java.util.Map;

@Service
public class GetTokenServiceImpl implements GetTokenService {

    @Autowired
    private GllUserDetailsService userDetailsService;

    @Override
    public String getToken(String token, String deviceId) throws Exception {
        if (token == null)
            return null;
        GllUser user = userDetailsService.getUserDetailsByVkToken(token, deviceId);
        Map<String, Object> tokenData = new HashMap<>();
        if (user != null) {
            tokenData.put("clientType", "user");
            tokenData.put("userID", user.getId().toString());
            tokenData.put("username", user.getUsername());
            tokenData.put("token_create_date", Instant.now().toEpochMilli());
            //TODO refactor
            Calendar calendar = Calendar.getInstance();
            calendar.add(Calendar.YEAR, 100);
            tokenData.put("token_expiration_date", calendar.getTime());
            JwtBuilder jwtBuilder = Jwts.builder();
            jwtBuilder.setExpiration(calendar.getTime());
            jwtBuilder.setClaims(tokenData);
            String key = "abc123";
            String jwtToken = jwtBuilder.signWith(SignatureAlgorithm.HS512, key).compact();
            return jwtToken;
        } else {
            throw new Exception("Authentication error");
        }
    }

}
