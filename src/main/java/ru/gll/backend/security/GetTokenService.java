package ru.gll.backend.security;

public interface GetTokenService {
    String getToken(String vkToken, String deviceId) throws Exception;
}
