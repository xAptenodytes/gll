package ru.gll.backend;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class BackendApplication {
	//https://www.callicoder.com/spring-boot-security-oauth2-social-login-part-2/

	public static void main(String[] args) {
		SpringApplication.run(BackendApplication.class, args);
	}
}

