package ru.gll.backend.dao;

import ru.gll.backend.models.CityModel;

import java.util.List;

public interface DataDao {
    List<CityModel> getCities();

    CityModel getCity(Integer cityId);
}
