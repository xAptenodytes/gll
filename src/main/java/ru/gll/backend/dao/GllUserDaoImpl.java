package ru.gll.backend.dao;

import org.hibernate.Session;
import org.springframework.stereotype.Repository;
import org.springframework.util.CollectionUtils;
import ru.gll.backend.models.GllUser;

import javax.transaction.Transactional;
import java.util.List;

@Repository
public class GllUserDaoImpl extends Dao implements GllUserDao {

    @Override
    public GllUser getUserByVkId(Integer vkId) {
        GllUser result = null;
        try (Session session = getSession()) {
            List<GllUser> preResult = session.createQuery(
                    "select gu from GllUser gu join gu.oauthUserCredentials uc " +
                            "WHERE uc.vkId = :vkId")
                    .setParameter("vkId", vkId)
                    .getResultList();
            if (!CollectionUtils.isEmpty(preResult)) {
                result = preResult.get(0);
            }
        } catch (Exception e) {
            e.printStackTrace();
            throw new RuntimeException("db error: " + e.getMessage());
        }
        return result;
    }

    @Override
    @Transactional
    public void saveOrUpdateUser(GllUser gllUser) {
        try (Session session = getSession()) {
            session.beginTransaction();
            session.saveOrUpdate(gllUser);
            session.getTransaction().commit();
        } catch (Exception e) {
            e.printStackTrace();
            throw new RuntimeException("db error: " + e.getMessage());
        }
    }
}
