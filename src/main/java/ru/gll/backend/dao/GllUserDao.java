package ru.gll.backend.dao;

import ru.gll.backend.models.GllUser;

import javax.transaction.Transactional;

public interface GllUserDao {

    GllUser getUserByVkId(Integer vkId);

    @Transactional
    void saveOrUpdateUser(GllUser gllUser);
}
