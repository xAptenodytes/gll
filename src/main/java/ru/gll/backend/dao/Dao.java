package ru.gll.backend.dao;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import javax.persistence.EntityManagerFactory;

@Repository
public class Dao {
    @Autowired
    private EntityManagerFactory entityManagerFactory;

    protected Session getSession() {
        return entityManagerFactory.unwrap(SessionFactory.class).openSession();
    }

    protected Session getCurrentSession() {
        return entityManagerFactory.unwrap(SessionFactory.class).getCurrentSession();
    }
}
