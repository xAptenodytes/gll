package ru.gll.backend.dao;

import org.hibernate.Session;
import org.springframework.stereotype.Repository;
import org.springframework.util.CollectionUtils;
import ru.gll.backend.models.CityModel;

import java.util.List;

@Repository
public class DataDaoImpl extends Dao implements DataDao {

    public List<CityModel> getCities() {
        List<CityModel> result = null;
        try (Session session = getSession()) {
            result = session.createQuery(
                    "from CityModel")
                    .getResultList();
        } catch (Exception e) {
            e.printStackTrace();
            throw new RuntimeException("db error: " + e.getMessage());
        }
        return result;
    }

    public CityModel getCity(Integer cityId) {
        CityModel result = null;
        try (Session session = getSession()) {
            List<CityModel> preResult = session.createQuery(
                    "from CityModel " +
                            "WHERE id = :cityId")
                    .setParameter("cityId", cityId)
                    .getResultList();
            if (!CollectionUtils.isEmpty(preResult)) {
                result = preResult.get(0);
            }
        } catch (Exception e) {
            e.printStackTrace();
            throw new RuntimeException("db error: " + e.getMessage());
        }
        return result;
    }
}
