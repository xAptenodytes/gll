package ru.gll.backend.controllers;

import com.vk.api.sdk.objects.base.Sex;
import io.swagger.annotations.Api;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RestController;
import ru.gll.backend.models.GllUser;
import ru.gll.backend.security.jwt.JwtTokenProvider;
import ru.gll.backend.services.DataService;
import ru.gll.backend.services.GllUserDetailsService;
import ru.gll.backend.swagger.controllers.MobileClientApi;
import ru.gll.backend.swagger.models.City;
import ru.gll.backend.swagger.models.UserData;

import java.time.LocalDate;
import java.util.List;

import static org.springframework.http.ResponseEntity.ok;

@Api(value = "Data", tags = "MobileClient")
@RestController
public class MobileClientController implements MobileClientApi {

    @Autowired
    private DataService dataService;
    @Autowired
    private GllUserDetailsService gllUserDetailsService;
    @Autowired
    private JwtTokenProvider jwtTokenProvider;

    private GllUser getGllUserByJwtToken(String authorizationToken) {
        String bearerToken = jwtTokenProvider.parseBearerToken(authorizationToken);
        return (GllUser) gllUserDetailsService
                .loadUserByUsername(jwtTokenProvider.getUsername(bearerToken));
    }

    @Override
    public ResponseEntity<List<City>> getCities() {
        return ok(dataService.getCities());
    }

    @Override
    public ResponseEntity<UserData> getUserData(String authorization) {
        GllUser user = getGllUserByJwtToken(authorization);
        UserData data = new UserData()
                .firstName(user.getFirstName())
                .patronymic(user.getPatronymic())
                .lastName(user.getLastName())
                .email(user.getEmail())
                .phone(user.getPhone())
                .birthday(user.getBirthday())
                .vkId(user.getOauthUserCredentials().getVkId());
        if (user.getCity() != null)
            data.setCity(user.getCity().getSwaggerCity());
        if (user.getSex() != null)
            data.setSex(user.getSex().getValue());
        return ok(data);
    }

    @Override
    public ResponseEntity<Void> setCity(String authorization, Integer cityId) {
        GllUser user = getGllUserByJwtToken(authorization);
        gllUserDetailsService.setUserCity(user, cityId);
        return new ResponseEntity(HttpStatus.OK);
    }

    @Override
    public ResponseEntity<Void> setUserBirthDay(String authorization, LocalDate birthday) {
        GllUser user = getGllUserByJwtToken(authorization);
        gllUserDetailsService.setUserBirthDay(user, birthday);
        return new ResponseEntity(HttpStatus.OK);
    }

    @Override
    public ResponseEntity<Void> updateUserPhone(String authorization, String phone) {
        GllUser user = getGllUserByJwtToken(authorization);
        gllUserDetailsService.updateUserPhone(user, phone);
        return new ResponseEntity(HttpStatus.OK);
    }

    @Override
    public ResponseEntity<Void> updateUserEmail(String authorization, String email) {
        GllUser user = getGllUserByJwtToken(authorization);
        gllUserDetailsService.updateUserEmail(user, email);
        return new ResponseEntity(HttpStatus.OK);
    }

    @Override
    public ResponseEntity<Void> updateUserProfileDetails(String authorization, String firstName, String lastName,
                                                         String patronymic, Integer sex) {
        GllUser user = getGllUserByJwtToken(authorization);
        Sex userSex;
        switch (sex) {
            case 0:
                userSex = Sex.UNKNOWN;
                break;
            case 1:
                userSex = Sex.FEMALE;
                break;
            case 2:
                userSex = Sex.MALE;
                break;
            default:
                userSex = null;
                break;
        }
        gllUserDetailsService.updateUserProfileDetails(user, firstName, patronymic, lastName, userSex);
        return new ResponseEntity(HttpStatus.OK);
    }

    @Override
    public ResponseEntity<Boolean> verifyCity(Integer cityId) {
        return ok(dataService.verifyCity(cityId));
    }
}
