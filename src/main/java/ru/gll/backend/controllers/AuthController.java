package ru.gll.backend.controllers;

import io.swagger.annotations.Api;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.authentication.BadCredentialsException;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.web.bind.annotation.RestController;
import ru.gll.backend.models.GllUser;
import ru.gll.backend.security.jwt.JwtTokenProvider;
import ru.gll.backend.services.GllUserDetailsService;
import ru.gll.backend.swagger.controllers.AuthApi;
import ru.gll.backend.swagger.models.JwtToken;
import ru.gll.backend.swagger.models.TokenType;

import static org.springframework.http.ResponseEntity.ok;

@Api(value = "Auth", tags = "Auth")
@RestController
public class AuthController implements AuthApi {

    @Autowired
    private GllUserDetailsService gllUserDetailsService;

    @Autowired
    private JwtTokenProvider jwtTokenProvider;

    @Override
    public ResponseEntity<JwtToken> signIn(String auth0TokenType, String auth0Token, String deviceId) {
        TokenType tokenType = TokenType.fromValue(auth0TokenType);
        switch (tokenType) {
            case VK:
                return ok(signInVkToken(auth0Token, deviceId));
            case GOOGLE:
                return new ResponseEntity<>(null, HttpStatus.NOT_IMPLEMENTED);
            default:
                return new ResponseEntity<>(null, HttpStatus.NOT_IMPLEMENTED);
        }
    }

    private JwtToken signInVkToken(String vkToken, String deviceId) {
        try {
            GllUser user = gllUserDetailsService.getUserDetailsByVkToken(vkToken, deviceId);
            Authentication authentication = new UsernamePasswordAuthenticationToken(user, null, user.getAuthorities());
            SecurityContextHolder.getContext().setAuthentication(authentication);
            String token = jwtTokenProvider.createToken(user.getUsername(), gllUserDetailsService.getUserRoles(user));

            JwtToken response = new JwtToken().username(user.getUsername()).token(token);
            return response;
        } catch (AuthenticationException e) {
            throw new BadCredentialsException("Invalid vk token");
        }
    }
}
