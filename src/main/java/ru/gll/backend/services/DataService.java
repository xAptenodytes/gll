package ru.gll.backend.services;

import ru.gll.backend.swagger.models.City;

import java.util.List;

public interface DataService {
    List<City> getCities();

    Boolean verifyCity(Integer cityId);
}
