package ru.gll.backend.services;

import com.vk.api.sdk.client.TransportClient;
import com.vk.api.sdk.client.VkApiClient;
import com.vk.api.sdk.client.actors.ServiceActor;
import com.vk.api.sdk.httpclient.HttpTransportClient;
import com.vk.api.sdk.objects.users.UserXtrCounters;
import com.vk.api.sdk.queries.users.UserField;
import com.vk.api.sdk.queries.users.UsersGetQuery;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.CollectionUtils;
import ru.gll.backend.dao.GllUserDao;
import ru.gll.backend.models.GllUser;
import ru.gll.backend.models.OauthUserCredentials;
import ru.gll.backend.utils.GLLConfig;

import java.util.List;

@Service
public class VkServiceImpl implements VkService {

    @Autowired
    private GLLConfig gllConfig;
    @Autowired
    private GllUserDao gllUserDao;

    private VkApiClient vk;

    {
        TransportClient transportClient = HttpTransportClient.getInstance();
        vk = new VkApiClient(transportClient);
    }

    /**
     * @param token
     * @return user from vk api
     */
    public GllUser getGllUserByToken(String token, String deviceId) {
        //TODO return user from db
        ServiceActor serviceActor = new ServiceActor(gllConfig.getVkAppId(), token);

        UsersGetQuery usersGetQuery = vk.users().get(serviceActor);
        try {
            List<UserXtrCounters> execute = usersGetQuery
                    .fields(UserField.SEX)
                    .execute();
            if (!CollectionUtils.isEmpty(execute)) {

                GllUser gllUser = gllUserDao.getUserByVkId(execute.get(0).getId());
                if (gllUser == null) {
                    gllUser = new GllUser(new OauthUserCredentials(execute.get(0), token, deviceId), execute.get(0).getFirstName(),
                            execute.get(0).getLastName(), execute.get(0).getSex());
                    gllUserDao.saveOrUpdateUser(gllUser);
                }
                return gllUser;
            }
        } catch (Exception e) {
            e.printStackTrace();
            throw new IllegalArgumentException("VK authorization goes wrong");
        }

        return null;
    }
}
