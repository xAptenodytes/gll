package ru.gll.backend.services;

import ru.gll.backend.models.GllUser;

public interface VkService {
    GllUser getGllUserByToken(String token, String deviceId);
}
