package ru.gll.backend.services;

import com.vk.api.sdk.objects.base.Sex;
import org.springframework.security.core.userdetails.UserDetailsService;
import ru.gll.backend.models.GllUser;
import ru.gll.backend.swagger.models.City;

import java.time.LocalDate;
import java.util.List;

public interface GllUserDetailsService extends UserDetailsService {
    GllUser getUserDetailsByVkToken(String token, String deviceId);

    List<String> getUserRoles(GllUser user);

    void updateUserProfileDetails(GllUser user, String firstName, String patronymic, String lastName, Sex sex);

    void updateUserPhone(GllUser user, String phone);

    void updateUserEmail(GllUser user, String email);

    void setUserBirthDay(GllUser user, LocalDate birthDay);

    void setUserCity(GllUser user, Integer cityId);
}
