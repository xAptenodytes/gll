package ru.gll.backend.services;

import com.vk.api.sdk.objects.base.Sex;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import ru.gll.backend.dao.DataDao;
import ru.gll.backend.dao.GllUserDao;
import ru.gll.backend.errorhandling.exceptions.BirthdayAlreadySetException;
import ru.gll.backend.errorhandling.exceptions.ResourceNotFoundException;
import ru.gll.backend.models.CityModel;
import ru.gll.backend.models.GllUser;

import java.time.LocalDate;
import java.util.Collections;
import java.util.List;

@Service
public class GllUserDetailsServiceImpl implements GllUserDetailsService {

    @Autowired
    private VkService vkService;

    @Autowired
    private DataDao dataDao;

    @Autowired
    private GllUserDao gllUserDao;

    /**
     * @param vkId
     * @return gll user by vk id
     */
    @Override
    public GllUser loadUserByUsername(String vkId) {
        return gllUserDao.getUserByVkId(Integer.valueOf(vkId));
    }

    /**
     * @param token    vk token
     * @param deviceId device id
     * @return gll user by vk token
     */
    @Override
    public GllUser getUserDetailsByVkToken(String token, String deviceId) {
        return vkService.getGllUserByToken(token, deviceId);
    }

    @Override
    public List<String> getUserRoles(GllUser user) {
        if (user.getRole() != null) {
            return Collections.singletonList(user.getRole().getName());
        }
        return Collections.singletonList("ORDINARY");
    }

    @Override
    public void updateUserProfileDetails(GllUser user, String firstName, String patronymic, String lastName, Sex sex) {
        user.setFirstName(firstName);
        user.setPatronymic(patronymic);
        user.setLastName(lastName);
        user.setSex(sex);
        gllUserDao.saveOrUpdateUser(user);
    }

    @Override
    public void updateUserPhone(GllUser user, String phone) {
        //TODO добавить проверку на валидность
        user.setPhone(phone);
        gllUserDao.saveOrUpdateUser(user);
    }

    @Override
    public void updateUserEmail(GllUser user, String email) {
        //TODO добавить проверку на валидность
        user.setEmail(email);
        gllUserDao.saveOrUpdateUser(user);
    }

    @Override
    public void setUserBirthDay(GllUser user, LocalDate birthDay) {
        if (user.getBirthday() == null) {
            user.setBirthday(birthDay);
            gllUserDao.saveOrUpdateUser(user);
        } else {
            throw new BirthdayAlreadySetException("Gll User's birthday already set");
        }
    }

    @Override
    public void setUserCity(GllUser user, Integer cityId) {
        CityModel city = dataDao.getCity(cityId);
        if (city == null) {
            throw new ResourceNotFoundException("city with id " + cityId + " wasn't found" );
        }
        user.setCity(city);
        gllUserDao.saveOrUpdateUser(user);
    }
}
