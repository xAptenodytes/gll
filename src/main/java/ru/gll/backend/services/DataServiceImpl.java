package ru.gll.backend.services;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import ru.gll.backend.dao.DataDao;
import ru.gll.backend.models.CityModel;
import ru.gll.backend.swagger.models.City;

import java.util.List;
import java.util.stream.Collectors;

@Service
public class DataServiceImpl implements DataService {
    @Autowired
    DataDao dataDao;

    @Override
    public List<City> getCities() {
        List<CityModel> cities = dataDao.getCities();
        return cities.stream()
                .map(city -> new City()
                        .cityId(city.getId())
                        .cityName(city.getName()))
                .collect(Collectors.toList());
    }

    @Override
    public Boolean verifyCity(Integer cityId) {
        return dataDao.getCity(cityId) != null;
    }
}
