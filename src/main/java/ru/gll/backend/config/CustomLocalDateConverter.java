package ru.gll.backend.config;

import org.springframework.core.convert.converter.Converter;
import org.springframework.stereotype.Component;

import java.time.LocalDate;

@Component
public class CustomLocalDateConverter implements Converter<String, LocalDate> {
    @Override
    public LocalDate convert(final String source) {
        if (source != null && !source.trim().isEmpty()) {
            return LocalDate.parse(source);
        }
        return null;
    }
}
