package ru.gll.backend.config;

import org.springframework.core.convert.converter.Converter;
import org.springframework.stereotype.Component;

import java.time.OffsetDateTime;

@Component
public class CustomOffsetDateTimeConverter implements Converter<String, OffsetDateTime> {

    @Override
    public OffsetDateTime convert(final String source) {
        if (source != null && !source.trim().isEmpty()) {
            return OffsetDateTime.parse(source);
        }
        return null;
    }
}
