package ru.gll.backend.config;

import org.springframework.boot.autoconfigure.domain.EntityScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;
import ru.gll.backend.models.GllUser;
import ru.gll.backend.models.OauthUserCredentials;

@EntityScan(basePackageClasses = {GllUser.class,
        OauthUserCredentials.class,
        ru.gll.backend.models.Role.class,
        ru.gll.backend.models.VkToken.class,
        ru.gll.backend.models.CityModel.class,
})
@Configuration
@EnableJpaRepositories
public class CommonConfiguration {
}
