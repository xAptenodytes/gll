package ru.gll.backend.errorhandling.exceptions;

public class ResourceNotFoundException extends RuntimeException {

    private static final long serialVersionUID = -8770177198536457066L;

    public ResourceNotFoundException(String message, Throwable cause) {
        super(message, cause);
    }

    public ResourceNotFoundException(String message) {
        super(message);
    }
}
