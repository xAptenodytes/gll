package ru.gll.backend.errorhandling.exceptions;

public class BirthdayAlreadySetException extends RuntimeException {
    public BirthdayAlreadySetException(String message, Throwable cause) {
        super(message, cause);
    }

    public BirthdayAlreadySetException(String message) {
        super(message);
    }
}
