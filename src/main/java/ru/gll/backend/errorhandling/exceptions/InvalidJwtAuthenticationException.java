package ru.gll.backend.errorhandling.exceptions;

public class InvalidJwtAuthenticationException extends RuntimeException {
    public InvalidJwtAuthenticationException(String message, Throwable cause) {
        super(message, cause);
    }
    public InvalidJwtAuthenticationException(String message) {
        super(message);
    }
}
