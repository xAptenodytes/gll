package ru.gll.backend.errorhandling;

import org.springframework.http.HttpStatus;
import org.springframework.http.converter.HttpMessageNotReadableException;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.MissingServletRequestParameterException;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.ResponseStatus;
import ru.gll.backend.errorhandling.exceptions.BirthdayAlreadySetException;
import ru.gll.backend.errorhandling.exceptions.ResourceNotFoundException;
import ru.gll.backend.errorhandling.exceptions.InvalidJwtAuthenticationException;
import ru.gll.backend.swagger.models.ErrorEntry;

import java.util.Arrays;
import java.util.List;

@ControllerAdvice
public class GllControllerAdvice {

    @ResponseStatus(HttpStatus.BAD_REQUEST)
    @ExceptionHandler({
            IllegalArgumentException.class,
            MissingServletRequestParameterException.class,
            HttpMessageNotReadableException.class,
            MethodArgumentNotValidException.class,
            InvalidJwtAuthenticationException.class,
            BirthdayAlreadySetException.class})
    @ResponseBody
    @SuppressWarnings("unused")
    List<ErrorEntry> handleBadRequest(Exception ex) {
        return makeErrorDataList(new ErrorEntry().code(ErrorData.PREFIX + ErrorData.DELIMITER +
                HttpStatus.BAD_REQUEST.value()).title(ex.getMessage()));
    }

    @ResponseStatus(HttpStatus.NOT_FOUND)
    @ExceptionHandler(ResourceNotFoundException.class)
    @ResponseBody
    @SuppressWarnings("unused")
    List<ErrorEntry> handleNotFound(Exception ex) {
        return makeErrorDataList(new ErrorEntry().code(ErrorData.PREFIX + ErrorData.DELIMITER +
                HttpStatus.NOT_FOUND.value()).title(ex.getMessage()));
    }

    @ResponseStatus(HttpStatus.INTERNAL_SERVER_ERROR)
    @ExceptionHandler(Exception.class)
    @ResponseBody
    @SuppressWarnings("unused")
    List<ErrorEntry> handleOthers(Exception ex) {
        return makeErrorDataList(new ErrorEntry().code(ErrorData.PREFIX + ErrorData.DELIMITER +
                HttpStatus.INTERNAL_SERVER_ERROR.value()).title(ex.getMessage()));
    }

    private List<ErrorEntry> makeErrorDataList(ErrorEntry... data) {
        return Arrays.asList(data);
    }
}
