package ru.gll.backend.errorhandling;

import lombok.AllArgsConstructor;
import lombok.Data;

@Data
@AllArgsConstructor
public class ErrorData {

    public static final String PREFIX = "GLL";
    public static final String DELIMITER = "-";

    private String code;
    private String title;
}
